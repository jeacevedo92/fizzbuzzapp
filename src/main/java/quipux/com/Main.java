package quipux.com;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese un número entero positivo (0 para salir):");

        while (true) {
            int number = scanner.nextInt();

            if (number == 0) {
                System.out.println("Saliendo de la aplicación.");
                break;
            }

            String result = fizzBuzz(number);
            System.out.println(result);
        }

        scanner.close();
    }

    public static String fizzBuzz(int number) {
        String output = "";

        if (number % 3 == 0) {
            output += "fizz";
        }

        if (number % 5 == 0) {
            output += "buzz";
        }

        return output.isEmpty() ? String.valueOf(number) : output;
    }
}
