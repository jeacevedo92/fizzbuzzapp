# FizzBuzzApp

## Descripción

FizzBuzzApp es una aplicación Java LTS desarrollada con Maven que implementa el popular algoritmo FizzBuzz.

## Requerimientos

- Java LTS (versión X.X.X)
- Maven (versión X.X.X)

## Instalación

1. Clona este repositorio en tu máquina local utilizando el siguiente comando:

```
git clone [URL del repositorio]
```

2. Navega al directorio del proyecto:

```
cd FizzBuzzApp
```

3. Compila el proyecto utilizando Maven:

```
mvn compile
```

## Uso

Para ejecutar la aplicación, sigue estos pasos:

1. Navega al directorio del proyecto:

```
cd FizzBuzzApp
```

2. Ejecuta la aplicación utilizando Maven:

```
mvn exec:java -Dexec.mainClass="com.example.FizzBuzzApp"
```

3. Sigue las instrucciones en pantalla para ingresar el rango de números y ver el resultado de FizzBuzz.

## Contribución

¡Contribuciones son bienvenidas! Si quieres contribuir a este proyecto, sigue estos pasos:

1. Haz un fork del repositorio.
2. Crea una nueva rama con el nombre de tu feature: `git checkout -b mi-feature`
3. Realiza tus cambios y haz commits: `git commit -am 'Añadir nueva feature'`
4. Sube tu rama: `git push origin mi-feature`
5. Envía un pull request.

## Licencia

Este proyecto está licenciado bajo la [Licencia MIT](https://opensource.org/licenses/MIT).
